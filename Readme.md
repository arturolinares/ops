# Ops Tools

# Supporting Crates

- [any-range/](any-range/) - `AnyRange<T>` enum can hold any `Range*<T>` type
- [build-data/](build-data/) - Include build data in your program
- [build-data-test/](build-data-test/) - Integration test for `build-data` crate
- [permit/](permit/) - A struct for cancelling operations
- [rustls-pin/](rustls-pin/) - Server certificate pinning with `rustls`
- [safe-lock/](safe-lock/) - A lock struct with a `const fn` constructor and no `unsafe`
- [temp-dir/](temp-dir/) - Simple temporary directory with cleanup
- [temp-file/](temp-file/) - Simple temporary file with cleanup
